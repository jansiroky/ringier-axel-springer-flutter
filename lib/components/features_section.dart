import 'package:flutter/cupertino.dart';
import 'package:google_fonts/google_fonts.dart';

class FeaturesSection extends StatelessWidget {
  final List<Widget> children;

  const FeaturesSection({Key? key, required this.children}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 16.0),
      padding: const EdgeInsets.symmetric(vertical: 16.0),
      decoration: const BoxDecoration(
        border: Border.symmetric(
          horizontal: BorderSide(color: CupertinoColors.separator),
        ),
      ),
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          children: children,
        ),
      ),
    );
  }
}

class FeatureItem extends StatelessWidget {
  final String label;
  final Widget value;

  const FeatureItem({Key? key, required this.label, required this.value}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 18.0),
      decoration: const BoxDecoration(
        border: Border(
          right: BorderSide(color: CupertinoColors.separator),
        ),
      ),
      child: Column(
        children: [
          Text(
            label,
            style: CupertinoTheme.of(context).textTheme.actionTextStyle,
          ),
          Padding(
            padding: const EdgeInsets.all(6.0),
            child: DefaultTextStyle(
              style: GoogleFonts.lora(
                  textStyle: CupertinoTheme.of(context)
                      .textTheme
                      .textStyle
                      .copyWith(fontSize: 22.0, fontWeight: FontWeight.bold)),
              child: value,
            ),
          ),
        ],
      ),
    );
  }
}
