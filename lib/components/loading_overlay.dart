import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LoadingOverlay extends StatelessWidget {
  final bool isLoading;

  const LoadingOverlay({Key? key, required this.isLoading}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedOpacity(
      opacity: isLoading ? 1.0 : 0.0,
      duration: Duration(milliseconds: 100),
      child: IgnorePointer(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                  color: CupertinoColors.systemGrey5,
                  borderRadius: BorderRadius.circular(12.0),
                ),
                padding: EdgeInsets.symmetric(vertical: 24.0, horizontal: 32.0),
                child: Column(
                  children: const [
                    Padding(
                      padding: EdgeInsets.only(bottom: 16.0),
                      child: CupertinoActivityIndicator(
                        radius: 16.0,
                      ),
                    ),
                    Text('Načítava sa...'),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
