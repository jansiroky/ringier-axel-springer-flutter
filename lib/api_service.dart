import 'package:http/http.dart' as http;

const BASE_URL = 'https://api.itbook.store/1.0/';

class ApiService {
  static Future<http.Response> newReleasesBooks() {
    return http.post(Uri.parse(BASE_URL + '/new'));
  }

  static Future<http.Response> searchBooks(String query, int page) {
    return http.post(Uri.parse(BASE_URL + '/search/$query/$page'));
  }

  static Future<http.Response> getBookDetails(String isbn13) {
    return http.post(Uri.parse(BASE_URL + '/books/$isbn13'));
  }
}
