library book;

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'book.g.dart';

abstract class Book implements Built<Book, BookBuilder> {
  String? get title;

  String? get subtitle;

  String get isbn13;

  String? get price;

  String? get image;

  String? get url;

  Book._();

  factory Book([void Function(BookBuilder) updates]) = _$Book;

  static Serializer<Book> get serializer => _$bookSerializer;
}
