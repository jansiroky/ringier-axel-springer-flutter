library book_detail;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:job_assignment/models/serializers.dart';

part 'book_detail.g.dart';

abstract class BookDetail implements Built<BookDetail, BookDetailBuilder> {
  String? get error;

  String? get title;

  String? get subtitle;

  String? get authors;

  String? get publisher;

  String? get language;

  String? get isbn10;

  String? get isbn13;

  String? get pages;

  String? get year;

  String? get rating;

  String? get desc;

  String? get price;

  String? get image;

  String? get url;

  BuiltMap<String, String>? get pdf;

  String toJson() {
    return json.encode(serializers.serializeWith(BookDetail.serializer, this));
  }

  static BookDetail? fromJson(String jsonString) {
    return serializers.deserializeWith(BookDetail.serializer, json.decode(jsonString));
  }

  BookDetail._();

  factory BookDetail([void Function(BookDetailBuilder) updates]) = _$BookDetail;

  static Serializer<BookDetail> get serializer => _$bookDetailSerializer;
}
