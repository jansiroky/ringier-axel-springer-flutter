library serializers;

import 'package:built_collection/built_collection.dart';
import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';
import 'package:job_assignment/models/book.dart';
import 'package:job_assignment/models/book_detail.dart';
import 'package:job_assignment/models/search_response.dart';

part 'serializers.g.dart';

@SerializersFor(const [
  SearchResponse,
  BookDetail,
])
final Serializers serializers = (_$serializers.toBuilder()..addPlugin(new StandardJsonPlugin())).build();
