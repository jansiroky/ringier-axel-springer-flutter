library search_response;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:job_assignment/models/book.dart';
import 'package:job_assignment/models/serializers.dart';

part 'search_response.g.dart';

abstract class SearchResponse implements Built<SearchResponse, SearchResponseBuilder> {
  String? get total;

  String? get page;

  BuiltList<Book>? get books;

  String toJson() {
    return json.encode(serializers.serializeWith(SearchResponse.serializer, this));
  }

  static SearchResponse? fromJson(String jsonString) {
    return serializers.deserializeWith(SearchResponse.serializer, json.decode(jsonString));
  }

  SearchResponse._();

  factory SearchResponse([void Function(SearchResponseBuilder) updates]) = _$SearchResponse;

  static Serializer<SearchResponse> get serializer => _$searchResponseSerializer;
}
