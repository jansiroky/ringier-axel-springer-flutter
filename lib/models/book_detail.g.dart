// GENERATED CODE - DO NOT MODIFY BY HAND

part of book_detail;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<BookDetail> _$bookDetailSerializer = new _$BookDetailSerializer();

class _$BookDetailSerializer implements StructuredSerializer<BookDetail> {
  @override
  final Iterable<Type> types = const [BookDetail, _$BookDetail];
  @override
  final String wireName = 'BookDetail';

  @override
  Iterable<Object?> serialize(Serializers serializers, BookDetail object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.error;
    if (value != null) {
      result
        ..add('error')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.title;
    if (value != null) {
      result
        ..add('title')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.subtitle;
    if (value != null) {
      result
        ..add('subtitle')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.authors;
    if (value != null) {
      result
        ..add('authors')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.publisher;
    if (value != null) {
      result
        ..add('publisher')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.language;
    if (value != null) {
      result
        ..add('language')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.isbn10;
    if (value != null) {
      result
        ..add('isbn10')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.isbn13;
    if (value != null) {
      result
        ..add('isbn13')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.pages;
    if (value != null) {
      result
        ..add('pages')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.year;
    if (value != null) {
      result
        ..add('year')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.rating;
    if (value != null) {
      result
        ..add('rating')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.desc;
    if (value != null) {
      result
        ..add('desc')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.price;
    if (value != null) {
      result
        ..add('price')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.image;
    if (value != null) {
      result
        ..add('image')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.url;
    if (value != null) {
      result
        ..add('url')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.pdf;
    if (value != null) {
      result
        ..add('pdf')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(BuiltMap,
                const [const FullType(String), const FullType(String)])));
    }
    return result;
  }

  @override
  BookDetail deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new BookDetailBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'error':
          result.error = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'title':
          result.title = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'subtitle':
          result.subtitle = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'authors':
          result.authors = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'publisher':
          result.publisher = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'language':
          result.language = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'isbn10':
          result.isbn10 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'isbn13':
          result.isbn13 = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'pages':
          result.pages = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'year':
          result.year = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'rating':
          result.rating = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'desc':
          result.desc = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'price':
          result.price = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'image':
          result.image = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'url':
          result.url = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'pdf':
          result.pdf.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltMap,
                  const [const FullType(String), const FullType(String)]))!);
          break;
      }
    }

    return result.build();
  }
}

class _$BookDetail extends BookDetail {
  @override
  final String? error;
  @override
  final String? title;
  @override
  final String? subtitle;
  @override
  final String? authors;
  @override
  final String? publisher;
  @override
  final String? language;
  @override
  final String? isbn10;
  @override
  final String? isbn13;
  @override
  final String? pages;
  @override
  final String? year;
  @override
  final String? rating;
  @override
  final String? desc;
  @override
  final String? price;
  @override
  final String? image;
  @override
  final String? url;
  @override
  final BuiltMap<String, String>? pdf;

  factory _$BookDetail([void Function(BookDetailBuilder)? updates]) =>
      (new BookDetailBuilder()..update(updates)).build();

  _$BookDetail._(
      {this.error,
      this.title,
      this.subtitle,
      this.authors,
      this.publisher,
      this.language,
      this.isbn10,
      this.isbn13,
      this.pages,
      this.year,
      this.rating,
      this.desc,
      this.price,
      this.image,
      this.url,
      this.pdf})
      : super._();

  @override
  BookDetail rebuild(void Function(BookDetailBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  BookDetailBuilder toBuilder() => new BookDetailBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is BookDetail &&
        error == other.error &&
        title == other.title &&
        subtitle == other.subtitle &&
        authors == other.authors &&
        publisher == other.publisher &&
        language == other.language &&
        isbn10 == other.isbn10 &&
        isbn13 == other.isbn13 &&
        pages == other.pages &&
        year == other.year &&
        rating == other.rating &&
        desc == other.desc &&
        price == other.price &&
        image == other.image &&
        url == other.url &&
        pdf == other.pdf;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc(
                                        $jc(
                                            $jc(
                                                $jc(
                                                    $jc(
                                                        $jc(
                                                            $jc(
                                                                $jc(
                                                                    0,
                                                                    error
                                                                        .hashCode),
                                                                title.hashCode),
                                                            subtitle.hashCode),
                                                        authors.hashCode),
                                                    publisher.hashCode),
                                                language.hashCode),
                                            isbn10.hashCode),
                                        isbn13.hashCode),
                                    pages.hashCode),
                                year.hashCode),
                            rating.hashCode),
                        desc.hashCode),
                    price.hashCode),
                image.hashCode),
            url.hashCode),
        pdf.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('BookDetail')
          ..add('error', error)
          ..add('title', title)
          ..add('subtitle', subtitle)
          ..add('authors', authors)
          ..add('publisher', publisher)
          ..add('language', language)
          ..add('isbn10', isbn10)
          ..add('isbn13', isbn13)
          ..add('pages', pages)
          ..add('year', year)
          ..add('rating', rating)
          ..add('desc', desc)
          ..add('price', price)
          ..add('image', image)
          ..add('url', url)
          ..add('pdf', pdf))
        .toString();
  }
}

class BookDetailBuilder implements Builder<BookDetail, BookDetailBuilder> {
  _$BookDetail? _$v;

  String? _error;
  String? get error => _$this._error;
  set error(String? error) => _$this._error = error;

  String? _title;
  String? get title => _$this._title;
  set title(String? title) => _$this._title = title;

  String? _subtitle;
  String? get subtitle => _$this._subtitle;
  set subtitle(String? subtitle) => _$this._subtitle = subtitle;

  String? _authors;
  String? get authors => _$this._authors;
  set authors(String? authors) => _$this._authors = authors;

  String? _publisher;
  String? get publisher => _$this._publisher;
  set publisher(String? publisher) => _$this._publisher = publisher;

  String? _language;
  String? get language => _$this._language;
  set language(String? language) => _$this._language = language;

  String? _isbn10;
  String? get isbn10 => _$this._isbn10;
  set isbn10(String? isbn10) => _$this._isbn10 = isbn10;

  String? _isbn13;
  String? get isbn13 => _$this._isbn13;
  set isbn13(String? isbn13) => _$this._isbn13 = isbn13;

  String? _pages;
  String? get pages => _$this._pages;
  set pages(String? pages) => _$this._pages = pages;

  String? _year;
  String? get year => _$this._year;
  set year(String? year) => _$this._year = year;

  String? _rating;
  String? get rating => _$this._rating;
  set rating(String? rating) => _$this._rating = rating;

  String? _desc;
  String? get desc => _$this._desc;
  set desc(String? desc) => _$this._desc = desc;

  String? _price;
  String? get price => _$this._price;
  set price(String? price) => _$this._price = price;

  String? _image;
  String? get image => _$this._image;
  set image(String? image) => _$this._image = image;

  String? _url;
  String? get url => _$this._url;
  set url(String? url) => _$this._url = url;

  MapBuilder<String, String>? _pdf;
  MapBuilder<String, String> get pdf =>
      _$this._pdf ??= new MapBuilder<String, String>();
  set pdf(MapBuilder<String, String>? pdf) => _$this._pdf = pdf;

  BookDetailBuilder();

  BookDetailBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _error = $v.error;
      _title = $v.title;
      _subtitle = $v.subtitle;
      _authors = $v.authors;
      _publisher = $v.publisher;
      _language = $v.language;
      _isbn10 = $v.isbn10;
      _isbn13 = $v.isbn13;
      _pages = $v.pages;
      _year = $v.year;
      _rating = $v.rating;
      _desc = $v.desc;
      _price = $v.price;
      _image = $v.image;
      _url = $v.url;
      _pdf = $v.pdf?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(BookDetail other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$BookDetail;
  }

  @override
  void update(void Function(BookDetailBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$BookDetail build() {
    _$BookDetail _$result;
    try {
      _$result = _$v ??
          new _$BookDetail._(
              error: error,
              title: title,
              subtitle: subtitle,
              authors: authors,
              publisher: publisher,
              language: language,
              isbn10: isbn10,
              isbn13: isbn13,
              pages: pages,
              year: year,
              rating: rating,
              desc: desc,
              price: price,
              image: image,
              url: url,
              pdf: _pdf?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'pdf';
        _pdf?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'BookDetail', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
