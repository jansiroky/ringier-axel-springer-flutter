import 'package:async_redux/async_redux.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:job_assignment/screens/home_screen.dart';
import 'package:job_assignment/state/app_state.dart';
import 'package:provider_for_redux/provider_for_redux.dart';

var state = AppState.initialState();

var store = Store<AppState>(
  initialState: state,
);

final navigatorKey = GlobalKey<NavigatorState>();

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  NavigateAction.setNavigatorKey(navigatorKey);

  runApp(const BookStoreApp());
}

class BookStoreApp extends StatelessWidget {
  const BookStoreApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AsyncReduxProvider<AppState>.value(
      value: store,
      child: CupertinoApp(
        title: 'IT Bookstore app',
        navigatorKey: navigatorKey,
        locale: const Locale('sk'),
        localizationsDelegates: const <LocalizationsDelegate<dynamic>>[
          GlobalMaterialLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        supportedLocales: const <Locale>[
          Locale('sk'),
        ],
        theme: const CupertinoThemeData(
          primaryColor: Colors.indigo,
        ),
        home: UserExceptionDialog<AppState>(
          child: const HomeScreen(),
        ),
      ),
    );
  }
}
