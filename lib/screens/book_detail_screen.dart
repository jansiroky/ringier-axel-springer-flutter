import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:job_assignment/components/features_section.dart';
import 'package:job_assignment/components/loading_overlay.dart';
import 'package:job_assignment/main.dart';
import 'package:job_assignment/models/book_detail.dart';
import 'package:job_assignment/state/app_state.dart';
import 'package:job_assignment/state/books_state.dart';
import 'package:provider_for_redux/provider_for_redux.dart';

class BookDetailScreen extends StatefulWidget {
  final String bookId;
  final String? bookImage;
  final String? bookTitle;

  const BookDetailScreen({Key? key, required this.bookId, required this.bookImage, required this.bookTitle})
      : super(key: key);

  @override
  _BookDetailScreenState createState() => _BookDetailScreenState();
}

class _BookDetailScreenState extends State<BookDetailScreen> {
  @override
  void initState() {
    super.initState();
    loadDetail();
  }

  void loadDetail() {
    store.dispatch(GetBookDetailAction(widget.bookId));
  }

  Future<void> onRefresh() async {
    loadDetail();
  }

  @override
  Widget build(BuildContext context) {
    return ReduxSelector<AppState, dynamic>(
      selector: (context, state) => [
        state.wait,
        state.booksState,
      ],
      builder: (context, store, state, dispatch, model, child) {
        final BookDetail? bookDetail = state.booksState.bookDetails[widget.bookId];
        return Stack(
          children: [
            CupertinoPageScaffold(
              navigationBar: CupertinoNavigationBar(
                padding: EdgeInsetsDirectional.zero,
                middle: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 12.0),
                  child: Text(
                    'Detail knihy',
                    style: GoogleFonts.lora(fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              child: SafeArea(
                bottom: false,
                child: CupertinoScrollbar(
                  child: CustomScrollView(
                    slivers: [
                      CupertinoSliverRefreshControl(
                        onRefresh: onRefresh,
                      ),
                      SliverToBoxAdapter(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 16.0),
                          child: Column(
                            children: <Widget>[
                              Hero(
                                tag: widget.bookId,
                                child: AspectRatio(
                                  aspectRatio: 0.8,
                                  child: CachedNetworkImage(
                                    imageUrl: bookDetail?.image ?? widget.bookImage ?? '',
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(bottom: 12.0),
                                child: Text(
                                  bookDetail?.title ?? widget.bookTitle ?? '',
                                  textAlign: TextAlign.center,
                                  style: GoogleFonts.lora(fontSize: 24.0, fontWeight: FontWeight.bold),
                                ),
                              ),
                              if (bookDetail != null) ...[
                                Padding(
                                  padding: const EdgeInsets.only(bottom: 12.0),
                                  child: Text(
                                    bookDetail.subtitle ?? '',
                                    textAlign: TextAlign.center,
                                    style: GoogleFonts.lora(fontSize: 18.0),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                                  child: Text(bookDetail.authors ?? '', textAlign: TextAlign.center),
                                ),
                                Padding(
                                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                                  child: Text(
                                    bookDetail.publisher ?? '',
                                    style: const TextStyle(
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                                FeaturesSection(
                                  children: [
                                    FeatureItem(
                                      label: 'Počet strán',
                                      value: Text((bookDetail.pages ?? '')),
                                    ),
                                    FeatureItem(
                                      label: 'Cena',
                                      value: Text((bookDetail.price ?? '')),
                                    ),
                                    FeatureItem(
                                      label: 'Rok vydania',
                                      value: Text((bookDetail.year ?? '')),
                                    ),
                                  ],
                                ),
                                Padding(
                                  padding: const EdgeInsets.symmetric(vertical: 16.0),
                                  child: Text(bookDetail.desc ?? ''),
                                ),
                                Padding(
                                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                                  child: SelectableText('ISBN13: ' + (bookDetail.isbn13 ?? '')),
                                ),
                                Padding(
                                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                                  child: SelectableText('ISBN10: ' + (bookDetail.isbn10 ?? '')),
                                ),
                              ],
                            ],
                          ),
                        ),
                      ),
                      SliverPadding(padding: EdgeInsets.all(80.0)),
                    ],
                  ),
                ),
              ),
            ),
            LoadingOverlay(isLoading: state.wait.isWaitingFor('GetBookDetailAction')),
          ],
        );
      },
    );
  }
}
