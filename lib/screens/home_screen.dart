import 'package:async_redux/async_redux.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_debounce/easy_debounce.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:job_assignment/components/loading_overlay.dart';
import 'package:job_assignment/main.dart';
import 'package:job_assignment/models/book.dart';
import 'package:job_assignment/screens/book_detail_screen.dart';
import 'package:job_assignment/state/app_state.dart';
import 'package:job_assignment/state/books_state.dart';
import 'package:provider_for_redux/provider_for_redux.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  GlobalKey navigationBarKey = GlobalKey();
  TextEditingController searchController = TextEditingController();

  bool get searchQueryEmpty => searchController.text.length < 2;

  @override
  void initState() {
    super.initState();
    store.dispatch(GetNewReleasedBooksAction());
  }

  void onChanged(String query) {
    if (query.length > 1) {
      EasyDebounce.debounce(
        'search',
        const Duration(milliseconds: 500),
        () => store.dispatch(GetSearchedBooksAction(query, 1)),
      );
    } else {
      setState(() {});
    }
  }

  Future<void> onRefresh() async {
    if (searchQueryEmpty) {
      store.dispatch(GetNewReleasedBooksAction());
    } else {
      onChanged(searchController.text);
    }
    return;
  }

  void openBookDetail(Book? book) {
    if (book != null) {
      store.dispatch(
        NavigateAction.push(
          CupertinoPageRoute(
              builder: (BuildContext context) => BookDetailScreen(
                    bookId: book.isbn13,
                    bookTitle: book.title,
                    bookImage: book.image,
                  )),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return ReduxSelector<AppState, dynamic>(
      selector: (context, state) => [
        state.wait,
        state.booksState,
      ],
      builder: (context, store, state, dispatch, model, child) {
        List<Book> books = searchQueryEmpty ? state.booksState.newReleasedBooks : state.booksState.searchedBooks;

        return Stack(
          children: [
            CupertinoPageScaffold(
              child: CupertinoScrollbar(
                child: CustomScrollView(
                  slivers: [
                    CupertinoSliverNavigationBar(
                      key: navigationBarKey,
                      padding: EdgeInsetsDirectional.zero,
                      middle: Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 12.0),
                            child: Text(
                              'Bookstore',
                              style: GoogleFonts.lora(fontWeight: FontWeight.bold, fontSize: 30.0),
                            ),
                          ),
                        ],
                      ),
                      largeTitle: Padding(
                        padding: const EdgeInsets.only(right: 16.0),
                        child: CupertinoSearchTextField(
                          controller: searchController,
                          onChanged: onChanged,
                          onSubmitted: onChanged,
                          padding: const EdgeInsetsDirectional.fromSTEB(8, 8, 5, 8),
                          prefixInsets: const EdgeInsetsDirectional.fromSTEB(10.0, 0.0, 0.0, 4.0),
                          placeholder: 'Vyhľadať',
                        ),
                      ),
                    ),
                    CupertinoSliverRefreshControl(
                      onRefresh: onRefresh,
                    ),
                    if (state.booksState.newReleasedBooks.isNotEmpty) ...[
                      SliverToBoxAdapter(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(16.0, 16.0, 0.0, 0.0),
                          child: Text(
                            searchQueryEmpty ? 'Nové vydania' : 'Výsledky vyhľadávania',
                            style: GoogleFonts.lora(fontSize: 24.0),
                          ),
                        ),
                      ),
                      if (!searchQueryEmpty && state.booksState.searchedBooks.isEmpty)
                        SliverToBoxAdapter(
                          child: SizedBox(
                            height: MediaQuery.of(context).size.height / 2,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: const <Widget>[
                                Padding(
                                  padding: EdgeInsets.only(bottom: 16.0),
                                  child: Icon(CupertinoIcons.doc_text_search, size: 48.0),
                                ),
                                Text(
                                  'Žiadne výsledky',
                                ),
                              ],
                            ),
                          ),
                        ),
                      SliverGrid(
                        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 3,
                          childAspectRatio: 0.6,
                        ),
                        delegate: SliverChildBuilderDelegate(
                          (context, index) {
                            Book book = books[index];
                            return Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: GestureDetector(
                                onTap: () => openBookDetail(book),
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    Hero(
                                      tag: book.isbn13,
                                      child: AspectRatio(
                                        aspectRatio: 0.8,
                                        child: book.image != null
                                            ? CachedNetworkImage(imageUrl: book.image!)
                                            : const SizedBox(),
                                      ),
                                    ),
                                    Text(
                                      book.title ?? '...',
                                      textAlign: TextAlign.center,
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 3,
                                      style: const TextStyle(fontSize: 14.0),
                                    ),
                                  ],
                                ),
                              ),
                            );
                          },
                          childCount: books.length,
                        ),
                      ),
                      const SliverPadding(padding: EdgeInsets.all(160.0)),
                    ],
                  ],
                ),
              ),
            ),
            LoadingOverlay(
              isLoading: state.wait.isWaitingFor('GetSearchedBooksAction') ||
                  state.wait.isWaitingFor('GetNewReleasedBooksAction'),
            ),
          ],
        );
      },
    );
  }
}
