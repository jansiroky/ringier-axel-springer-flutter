import 'package:async_redux/async_redux.dart';
import 'package:flutter/widgets.dart';
import 'package:job_assignment/state/books_state.dart';

@immutable
class AppState {
  final Wait wait;
  final BooksState booksState;

  AppState({
    required this.wait,
    required this.booksState,
  });

  AppState copy({
    Wait? wait,
    BooksState? booksState,
  }) {
    return AppState(
      wait: wait ?? this.wait,
      booksState: booksState ?? this.booksState,
    );
  }

  static AppState initialState() => AppState(
        wait: Wait(),
        booksState: BooksState.initialState(),
      );

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AppState && runtimeType == other.runtimeType && wait == other.wait && booksState == other.booksState;

  @override
  int get hashCode => wait.hashCode ^ booksState.hashCode;
}

abstract class BarrierAction<T> extends ReduxAction<AppState> {
  void before() => dispatch(WaitAction.add(T.toString()));

  void after() => dispatch(WaitAction.remove(T.toString()));
}
