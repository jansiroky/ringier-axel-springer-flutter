import 'package:async_redux/async_redux.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:job_assignment/api_service.dart';
import 'package:job_assignment/models/book.dart';
import 'package:job_assignment/models/book_detail.dart';
import 'package:job_assignment/models/search_response.dart';
import 'package:job_assignment/state/app_state.dart';

class BooksState {
  final int page;
  final List<Book> newReleasedBooks;
  final List<Book> searchedBooks;
  final Map<String, BookDetail> bookDetails;

  BooksState({
    required this.page,
    required this.newReleasedBooks,
    required this.searchedBooks,
    required this.bookDetails,
  });

  BooksState copy({
    int? page,
    List<Book>? newReleasedBooks,
    List<Book>? searchedBooks,
    Map<String, BookDetail>? bookDetails,
  }) {
    return BooksState(
      page: page ?? this.page,
      newReleasedBooks: newReleasedBooks ?? this.newReleasedBooks,
      searchedBooks: searchedBooks ?? this.searchedBooks,
      bookDetails: bookDetails ?? this.bookDetails,
    );
  }

  static BooksState initialState() => BooksState(
        page: 1,
        newReleasedBooks: const [],
        searchedBooks: const [],
        bookDetails: const {},
      );

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        other is BooksState &&
            runtimeType == other.runtimeType &&
            page == other.page &&
            listEquals(newReleasedBooks, other.newReleasedBooks) &&
            listEquals(searchedBooks, other.searchedBooks) &&
            mapEquals(bookDetails, other.bookDetails);
  }
}

class GetSearchedBooksAction extends BarrierAction<GetSearchedBooksAction> {
  final String query;
  final int page;

  GetSearchedBooksAction(this.query, this.page);

  @override
  Future<AppState?> reduce() async {
    try {
      http.Response response = await ApiService.searchBooks(query, page);
      if (response.statusCode != 200) {
        throw const UserException('Vyskytla sa chyba s načítaním dát.');
      }

      final SearchResponse? searchResponse = SearchResponse.fromJson(response.body);

      return state.copy(
        booksState: state.booksState.copy(
          searchedBooks: searchResponse?.books?.toList(),
        ),
      );
    } catch (e) {
      throw const UserException('Chyba', cause: 'Vyskytla sa chyba s načítaním dát.');
    }
  }
}

class GetNewReleasedBooksAction extends BarrierAction<GetNewReleasedBooksAction> {
  @override
  Future<AppState?> reduce() async {
    try {
      http.Response response = await ApiService.newReleasesBooks();
      if (response.statusCode != 200) {
        throw const UserException('Vyskytla sa chyba s načítaním dát.');
      }

      final SearchResponse? searchResponse = SearchResponse.fromJson(response.body);

      return state.copy(
        booksState: state.booksState.copy(
          newReleasedBooks: searchResponse?.books?.toList(),
        ),
      );
    } catch (e) {
      throw const UserException('Chyba', cause: 'Vyskytla sa chyba s načítaním dát.');
    }
  }
}

class GetBookDetailAction extends BarrierAction<GetBookDetailAction> {
  final String bookId;

  GetBookDetailAction(this.bookId);

  @override
  Future<AppState?> reduce() async {
    try {
      http.Response response = await ApiService.getBookDetails(bookId);
      if (response.statusCode != 200) {
        throw const UserException('Vyskytla sa chyba s načítaním dát.');
      }

      final BookDetail? bookDetail = BookDetail.fromJson(response.body);

      if (bookDetail != null) {
        return state.copy(
          booksState: state.booksState.copy(
            bookDetails: {
              ...state.booksState.bookDetails,
              bookId: bookDetail,
            },
          ),
        );
      } else {
        throw const UserException('Vyskytla sa chyba s deserializáciou dát.');
      }
    } catch (e) {
      throw const UserException('Chyba', cause: 'Vyskytla sa chyba s načítaním dát.');
    }
  }
}
